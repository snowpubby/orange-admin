package com.orangeforms.common.core.validator;

/**
 * 数据增加的验证分组。通常用于数据新增场景。
 *
 * @author Jerry
 * @date 2022-02-20
 */
public interface AddGroup {
}
